(********************************************************************
 * COPYRIGHT -- B&R Industrial Automation
 ********************************************************************
 * Library: etxitoa
 * File: etxMpRecipe.st
 * Author: Brad Jones
 * Created: October 17, 2015
 ********************************************************************
 * Implementation OF library etxitoa
 ********************************************************************) 

// etxMpRecipe, ver: 1.00.0
FUNCTION etxMpRecipe
	CASE (i_ErrorID) OF		
		0: 		i_ErrorIDText := 	'OK - No Error';
		1: 		i_ErrorIDText := 	'mpFILE_ERR_ACTIVATION (-1064329103): Could not create component';
		2: 		i_ErrorIDText := 	'mpFILE_ERR_MPLINK_NULL (-1064329102): MpLink is null pointer';
		3: 		i_ErrorIDText := 	'mpFILE_ERR_MPLINK_INVALID (-1064329101): MpLink connection not allowed';
		4: 		i_ErrorIDText := 	'mpFILE_ERR_MPLINK_CHANGED (-1064329100): MpLink modified';
		5: 		i_ErrorIDText := 	'mpFILE_ERR_MPLINK_CORRUPT (-1064329099): Invalid MpLink contents';
		6: 		i_ErrorIDText := 	'mpFILE_ERR_MPLINK_IN_USE (-1064239098): MpLink already in use';
		
		32769: 		i_ErrorIDText := 	'mpRECIPE_ERR_SAVE_DATA (-1064140799): Error saving recipe';
		32770: 		i_ErrorIDText := 	'mpRECIPE_ERR_LOAD_DATA (-1064140798): Error loading recipe';
		32771: 		i_ErrorIDText := 	'mpRECIPE_ERR_INVALID_FILE_DEV (-1064140797): Invalid file device';
		32772: 		i_ErrorIDText := 	'mpRECIPE_ERR_INVALID_FILE_NAME (-1064140796): Invalid filename';
		32773: 		i_ErrorIDText := 	'mpRECIPE_ERR_CMD_IN_PROGRESS (-1064140795): Component currently executing command';
		32774: 		i_ErrorIDText := 	'mpRECIPE_WRN_SAVE_WITH_WARN (-2137882618): Warnings while saving recipe';
		32775: 		i_ErrorIDText := 	'mpRECIPE_WRN_LOAD_WITH_WARN (-2137882617): Warnings while loading recipe';
		32776: 		i_ErrorIDText := 	'mpRECIPE_ERR_SAVE_WITH_ERRORS (-1064140792): Error converting recipe parameters';
		32777: 		i_ErrorIDText := 	'mpRECIPE_ERR_LOAD_WITH_ERRORS (-1064140791): Error converting recipe to parameters ';
		32778: 		i_ErrorIDText := 	'mpRECIPE_ERR_MISSING_RECIPE (-1064140790): No connection to parent component';
		32779: 		i_ErrorIDText := 	'mpRECIPE_ERR_MISSING_MPFILE (-1064140789): No connection to file manager';
		32780: 		i_ErrorIDText := 	'mpRECIPE_ERR_INVALID_SORT_ORDER (-1064140788): Invalid sort order selected';
		32781: 		i_ErrorIDText := 	'mpRECIPE_WRN_MISSING_UICONNECT (-2137882611): Invalid value for "UIConnect"';
		32782: 		i_ErrorIDText := 	'mpRECIPE_ERR_INVALID_PV_NAME (-1064140786): Invalid PV';
		32783: 		i_ErrorIDText := 	'mpRECIPE_ERR_INVALID_LOAD_TYPE (-1064140785): Invalid LoadType';
		32784: 		i_ErrorIDText := 	'mpRECIPE_ERR_LISITING_FILES (-1064140784): Error creating file list';
		32785: 		i_ErrorIDText := 	'mpRECIPE_ERR_PV_NAME_NULL (-1064140783): PV name is NULL.';
		32786: 		i_ErrorIDText := 	'mpRECIPE_WRN_NO_PV_REGISTERED (-2137882606): No PV registered';
		32787: 		i_ErrorIDText := 	'mpRECIPE_ERR_SYNC_SAVE_ACTIVE (-1064140781): Saving active';
		32788: 		i_ErrorIDText := 	'mpRECIPE_ERR_DELETING_FILE (-1064140780): Error deleting file';
		32789: 		i_ErrorIDText := 	'mpRECIPE_WRN_EMPTY_RECIPE (-2137882603): Empty recipe created';
		32790: 		i_ErrorIDText := 	'mpRECIPE_INF_WAIT_RECIPE_FB (1083342870): MpRecipeXml not yet active';

		ELSE
			IDText := UDINT_TO_STRING(i_ErrorID);
			i_ErrorIDText := 	CONCAT('Unkown ErrorID: ',IDText);
	END_CASE  
END_FUNCTION
