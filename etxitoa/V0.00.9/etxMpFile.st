(********************************************************************
 * COPYRIGHT -- B&R Industrial Automation
 ********************************************************************
 * Library: etxitoa
 * File: etxMpFile.st
 * Author: Brad Jones
 * Created: October 17, 2015
 ********************************************************************
 * Implementation OF library etxitoa
 ********************************************************************) 

// etxMpFile, ver: 1.00.0
FUNCTION etxMpFile
	CASE (i_ErrorID) OF		
		0: 		i_ErrorIDText := 	'OK - No Error';
		1: 		i_ErrorIDText := 	'mpFILE_ERR_ACTIVATION (-1064329103): Could not create component';
		2: 		i_ErrorIDText := 	'mpFILE_ERR_MPLINK_NULL (-1064329102): MpLink is null pointer';
		3: 		i_ErrorIDText := 	'mpFILE_ERR_MPLINK_INVALID (-1064329101): MpLink connection not allowed';
		4: 		i_ErrorIDText := 	'mpFILE_ERR_MPLINK_CHANGED (-1064329100): MpLink modified';
		5: 		i_ErrorIDText := 	'mpFILE_ERR_MPLINK_CORRUPT (-1064329099): Invalid MpLink contents';
		6: 		i_ErrorIDText := 	'mpFILE_ERR_MPLINK_IN_USE (-1064239098): MpLink already in use';
		
		8192: 		i_ErrorIDText := 	'mpFILE_ERR_MISSING_UICONNECT (-1064165376): Missing value on UIConnect';
		8193: 		i_ErrorIDText := 	'mpFILE_ERR_CMD_NOT_ALLOWED (-1064165375): Command not allowed';
		8194: 		i_ErrorIDText := 	'mpFILE_ERR_NOTHING_TO_PASTE (-1064165374): No element to paste';
		8195: 		i_ErrorIDText := 	'mpFILE_ERR_NOTHING_SELECTED (-1064165373): Nothing selected';
		8196: 		i_ErrorIDText := 	'mpFILE_ERR_DIR_ALREADY_EXISTS (-1064165372): Folder already exists';
		8197: 		i_ErrorIDText := 	'mpFILE_ERR_INVALID_FILE_DEV (-1064165371): Invalid file device';
		8198: 		i_ErrorIDText := 	'mpFILE_ERR_NAME_EMPTY(-1064165370): New name not entered';
		8199: 		i_ErrorIDText := 	'mpFILE_ERR_INVALID_NAME (-1064165369): Invalid name';
		8200: 		i_ErrorIDText := 	'mpFILE_ERR_PASTE_NOT_ALLOWED (-1064165368): Paste not allowed';
	
	ELSE
	IDText := UDINT_TO_STRING(i_ErrorID);
	i_ErrorIDText := 	CONCAT('Unkown ErrorID: ',IDText);
	END_CASE    
END_FUNCTION
