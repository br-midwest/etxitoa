(********************************************************************
 * COPYRIGHT -- B&R Industrial Automation
 ********************************************************************
 * Library: etxitoa
 * File: etxitoa.fun
 * Author: Marcel Voigt
 * Created: October 06, 2015
 ********************************************************************
 * Functions and function blocks of library etxitoa
 ********************************************************************)

FUNCTION etxACOPOS : USINT (*TODO: Add your comment here*) (*$GROUP=User*)
	VAR_INPUT
		i_InternalErrorID : UDINT;
		i_ErrorIDText : REFERENCE TO STRING[255];
	END_VAR
	VAR
		IDText : STRING[80];
	END_VAR
END_FUNCTION

FUNCTION etxMpAxis : USINT (*TODO: Add your comment here*) (*$GROUP=User*)
	VAR_INPUT
		i_ErrorID : UDINT;
		i_ErrorIDText : REFERENCE TO STRING[255];
	END_VAR
	VAR
		IDText : STRING[80];
	END_VAR
END_FUNCTION

FUNCTION etxMpFile : USINT (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		i_ErrorID : UDINT;
		i_ErrorIDText : REFERENCE TO STRING[255];
	END_VAR
	VAR
		IDText : STRING[80];
	END_VAR
END_FUNCTION

FUNCTION etxMpRecipe : BOOL (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		i_ErrorID : UDINT;
		i_ErrorIDText : REFERENCE TO STRING[255];
	END_VAR
	VAR
		IDText : STRING[80];
	END_VAR
END_FUNCTION

FUNCTION etxMpMotionStatusID : USINT
	VAR_INPUT
		i_InternalErrorID : DINT;
		i_ErrorIDText : REFERENCE TO STRING[255];
	END_VAR
	VAR
		IDText : STRING[80];
	END_VAR
END_FUNCTION
