(********************************************************************
 * COPYRIGHT -- B&R Industrial Automation
 ********************************************************************
 * Library: etxitoa
 * File: etxMpAxis.st
 * Author: Marcel Voigt
 * Created: October 17, 2015
 ********************************************************************
 * Implementation of library etxitoa
 ********************************************************************) 

// etxMpAxis, ver: 1.00.0
FUNCTION etxMpAxis
	CASE i_ErrorID OF
		0: 		i_ErrorIDText := 	'OK - No Error';
		1:	 	i_ErrorIDText := 	'mpAXIS_ERR_ACTIVATION (-1064239103): Could not create component';
		2:		i_ErrorIDText := 	'mpAXIS_ERR_MPLINK_NULL (-1064239102): MpLink is null pointer';
		3:		i_ErrorIDText := 	'mpAXIS_ERR_MPLINK_INVALID (-1064239101): MpLink connection NOT allowed';
		4: 		i_ErrorIDText :=	'mpAXIS_ERR_MPLINK_CHANGED (-1064239100): MpLink modified';
		5: 		i_ErrorIDText :=	'mpAXIS_ERR_MPLINK_CORRUPT (-1064239099): Invalid MpLink contents';
		6: 		i_ErrorIDText :=	'mpAXIS_ERR_MPLINK_IN_USE (-1064239098): MpLink already in use';
		7: 		i_ErrorIDText :=	'mpAXIS_ERR_PAR_NULL (-1064239097): Parameter structure is a null pointer';
		8: 		i_ErrorIDText :=	'mpAXIS_ERR_CONFIG_NULL (-1064239096): Configuration structure is a null pointer';
		9: 		i_ErrorIDText :=	'mpAXIS_ERR_CONFIG_NO_PV (-1064239095): Configuration pointer NOT PV';
		10: 	i_ErrorIDText :=	'mpAXIS_ERR_CONFIG_LOAD (-1064239094): Error loading configuration';
		11:		i_ErrorIDText :=	'mpAXIS_WRN_CONFIG_LOAD (-2137980917): Warning loading configuration';
		12: 	i_ErrorIDText :=	'mpAXIS_ERR_CONFIG_SAVE (-1064239092): Error saving configuration';
		13: 	i_ErrorIDText :=	'mpAXIS_ERR_CONFIG_INVALID (-1064239091): Invalid configuration';
		33280: 	i_ErrorIDText :=	'mpAXIS_ERR_AXIS_HANDLE_NULL (-1064074752): Invalid reference';
		33281: 	i_ErrorIDText :=	'mpAXIS_WRN_ERROR_TABLE_MISSING (-2137816575): Missing error table';
		33283: 	i_ErrorIDText :=	'mpAXIS_WRN_CFG_WAIT_POWER_OFF (-2137816573): Configuration update only possible after axis in Disabled state';
		33284: 	i_ErrorIDText :=	'mpAXIS_WRN_CFG_WAIT_STANDSTILL (-2137816572): Configuration update only possible after axis in Standstill state';
		33285: 	i_ErrorIDText :=	'mpAXIS_ERR_PLC_OPEN (-1064074747): PLCopen error active';
		33286: 	i_ErrorIDText :=	'mpAXIS_WRN_PLC_OPEN (-2137816570): PLCopen warning active';
		33287: 	i_ErrorIDText :=	'mpAXIS_WRN_READ_TORQUE_OFF (-2137816569): Torque read FUNCTION disabled';
		33288: 	i_ErrorIDText :=	'mpAXIS_ERR_MAX_TORQUE_REACHED (-1064074744): Maximum torque reached';
		33300: 	i_ErrorIDText :=	'mpAXIS_ERR_SLAVE_NOT_FOUND (-1064074732): Slave NOT found';
		33301: 	i_ErrorIDText :=	'mpAXIS_ERR_MASTER_NOT_FOUND (-1064074731): Master NOT found';
		33302: 	i_ErrorIDText :=	'mpAXIS_ERR_WRONG_DENOMINATOR (-1064074730): Invalid denominator';
		33303: 	i_ErrorIDText :=	'mpAXIS_ERR_WRONG_NUMERATOR (-1064074729): Invalid numerator';
		33304: 	i_ErrorIDText :=	'mpAXIS_ERR_NO_CAM_NAME (-1064074728): No name FOR cam profile';
		33305: 	i_ErrorIDText :=	'mpAXIS_WRN_SLAVE_NOT_READY (-2137816551): Slave NOT ready';
		33306: 	i_ErrorIDText :=	'mpAXIS_ERR_CHECK_SLAVE_STATUS (-1064074726): Error on slave during last command';
		33307: 	i_ErrorIDText :=	'mpAXIS_ERR_CMD_WRONG_AXISTYPE (-1064074725): Invalid axis type FOR this command';
		33308: 	i_ErrorIDText :=	'mpAXIS_WRN_PARAMETER_LIMITED (-2137816551): Parameter was limited';
		33309: 	i_ErrorIDText :=	'mapAXIS_WRN_MULTIPLE_COMMAND (-2137816547): Multiple commands enabled simultaneously';
		33310: 	i_ErrorIDText :=	'mapAXIS_ERR_CAM_PARAMETER (-1064074722): Cam profile parameters';
		ELSE
			IDText := UDINT_TO_STRING(i_ErrorID);
			i_ErrorIDText := 	CONCAT('Unknown ErrorID: ',IDText);
	END_CASE;
END_FUNCTION
